#!/usr/bin/env python3
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from mines.minefield import Minefield, App

app = App(Minefield(9, 9, 9))
app.show()
Gtk.main()
