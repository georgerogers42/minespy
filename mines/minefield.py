#!/usr/bin/env pypy3
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from random import randint

class Cell(object):
    def __init__(self, safety=10):
        self.clicked = False
        self._bomb = not(bool(randint(0, safety)))
    @property
    def bomb(self):
        return self._bomb
    @property
    def complete(self):
        return self.bomb or self.clicked

class Minefield(object):
    def __init__(self, lives, rows, cols, safety=9):
        self._lives = lives
        self.rows = rows
        self.cols = cols
        self.mines = []
        for i in range(self.rows):
            row = []
            for j in range(self.cols):
                row.append(Cell())
            self.mines.append(row)
    @property
    def lives(self):
        return self._lives
    def __getitem__(self, idx):
        i, j = idx
        return self.mines[i][j]
    def __setitem__(self, idx, v):
        i, j = idx
        self.mines[i][j] = v
    def click(self, i, j):
        cell = self[i,j]
        if self[i, j].clicked:
            return self.safety(i, j)
        self[i, j].clicked = True
        if self[i, j].bomb:
            self._lives -= 1
            return 0
        return self.safety(i, j)
    def safety(self, i, j):
        if self[i, j].bomb:
            return 0
        safe = 9
        idxs = [(i-1, j-1), (i, j-1), (i+1, j-1),
                (i-1, j),   (i, j),   (i+1, j),
                (i-1, j+1), (i, j+1), (i+1, j+1)]
        for idx in idxs:
            if idx[0] >= 0 and idx[0] < self.rows and idx[1] >= 0 and idx[1] < self.cols:
                if self[idx].bomb:
                    safe -= 1
            else:
                safe -= 1
        return safe
    def __iter__(self):
        for row in self.mines:
            for cell in row:
                yield cell
    @property
    def complete(self):
        if self.lives <= 0:
            return True
        for cell in self:
            if not cell.complete:
                return False
        return True
    @property
    def blanks(self):
        return len([cell for cell in self if not cell.clicked])

class App(object):
    def __init__(self, minefield):
        self.minefield = minefield
        self.window = Gtk.Window(title="Minesweeper")
        rows = Gtk.VBox()
        self.lives = Gtk.Label()
        self.lives.set_text(str(self.minefield.lives))
        rows.add(self.lives)
        for i, row in enumerate(minefield.mines):
            cols = Gtk.HBox()
            for j, col in enumerate(row):
                btn = Gtk.Button(label=" ")
                btn.connect("clicked", self.click_handler(btn, i, j))
                cols.add(btn)
            rows.add(cols)
        self.window.add(rows)
        self.window.connect("destroy", Gtk.main_quit)
    def show(self):
        self.window.show_all()
    def click(self, btn, i, j, evt):
        if self.minefield.complete:
            return
        safety = self.minefield.click(i, j)
        if safety == 0:
            btn.set_label("*")
        else:
            btn.set_label(str(safety))
        if self.minefield.lives > 0 and self.minefield.complete:
            self.lives.set_text("You win with %d lives remaining and %d blanks" % (self.minefield.lives, self.minefield.blanks))
        elif self.minefield.lives > 0:
            self.lives.set_text("%2d" % self.minefield.lives)
        else:
            self.lives.set_text("Game Over")
            
    def click_handler(self, btn, i, j):
        return lambda evt: self.click(btn, i, j, evt)

